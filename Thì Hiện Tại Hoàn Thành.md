## Thì Hiện Tại Hoàn Thành

A. Cách Dùng 

- Diễn tả một hành động trong quá khứ nhưng không kéo dài đến hiện tại Eg: I  **have learned** English for 10 years.
- Diễn tả một hành động sảy ra trong quá khứ nhưng không rõ thời gian. EG: She **has lost** her bag
- Một hành động lặp đi lặp lại trong quá khứ (tính đến thời điểm hiện tại ) Eg: We ** have been** to Ha Noi 3 times.

B. Công thức

1. **Thể khẳng** định của thì thiện tại hoàn thành được tạo bởi hai thành phần là trợ động từ **have** và động từ miêu tả hành động ở dạng V3
- Trợ động từ have có 2 dạng have và has tùy vào chủ ngữ của nó:
 
 - I have (ngôi thứ nhất)
 - You have (ngôi thứ 2 số ít)
 - He has /She has / It has ( ngôi thứ 3 số ít)
 - We have ( ngôi thứ nhất số nhiều )
 - You have (ngôi thứ 2 số nhiều)
 - They have ( ngôi thứ 3 số nhiều)
 - 
  
**Động từ diễn tả hành động thì sẽ phải ở dạng V3**

 - Đôi với hầu hết các động từ, dạng V3 được hình thành dễ dàng bằng cách gắn đuôi -ed vào động từ nguyên mẫu :
 
  - cook => cooked
  - live => lived
  - need =>needed
  - play =>played
  
 - Tuy nhiên đối với các động từ bất quy tấc, dạng V3 yêu cầu chúng ta phải học thuộc lòng dạng đặc biệt của nó. Dưới đây là 10 động từ bất quy tắc cực kỳ phổ biến  mà bạn không thể không biết:
  - be => been
  - have => had
  - do =>done
  - go => goes
  - come => come
  - make => made
  - eat => eaten
  - drink => drunk
  - see => seen
  - sleep => slept
  - 
 Để tạo thể khẳng định của thì hiện tại hoàn thành, chúng ta cần làm hai bước :
 - Chuyển have thành have hoặc has tùy theo chủ ngữ
 - Chuyển động từ thành dạng V3
 
** Chủ ngữ + have/has + V3

- Thể Phủ Định: Chỉ cần thêm not vào sau từ have là xong

**Công THức: Chủ Ngữ + Have/has + not + V3

TIP: thỉnh thoảng  have not sẽ được viết haven't còn has not được viết là hasn't

2. Thể phủ định
Chỉ cần cần thêm not sau trợ động từ have là xong!

Công Thức : CHỦ NGỮ + have/has + not + V3
TIP:Thỉnh thoảng have not sẽ được viết tắt là haven't còn has not sẽ được viết tắt là hasn't.

Eg: They **haven't lived** here for years.
 - have được thay bằng  have vì chủ ngữ là ngôi thứ 3 số nhiều
 - live được thay bằng lived (V3 của lived)
 - not được thêm vào sau have để phủ định và được rút gọn vào have -> haven't
 
3. Thể nghi vấn

Có hai loại: Câu hỏi có từ để hỏi và câu hỏi yes/no

Đối với câu hỏi có từ để hỏi (như what, who, where,...) bạn chỉ cần đảo trợ động từ have ra trước chủ ngữ và sau từ hỏi

Công thức : TỪ ĐỂ HỎI + have/has + CHỦ NGỮ + V3

Eg: What**have** you **done** to my bag
 - have=> have vì chủ ngữ là **you** ngôi thứ 2 số ít 
 - do=> done (V3 của do)
 - have được đảo ra trước chủ ngữ you và sau từ để hỏi what

Đối với câu hỏi không có từ để hỏi chỉ cần đảo have ra trước chủ ngữ

Công Thức: have/has + CHỦ NGỮ + V3

