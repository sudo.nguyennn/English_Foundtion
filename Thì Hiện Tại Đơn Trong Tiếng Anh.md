## 1. Thì Hiện Tại Đơn

1.1 Cách dùng
- Một thói quen một điều diễn ra liên tục ở hiện tại Eg: she always **comes** to class early
- Việc đó diễn ra thường xuyên đến mức nào ( một tuần, hai tuần, vv) Eg: I often **go** the cinema once a week.
- Một chân lý một sự thật hiển nhiên Eg: The sun **rises** in the east. Eg: Tom **is** from England.
- Nghĩa tương lai: chương trình hoặc kế hoạch được sắp xếp sẵn trong tuong lai hoặc thời gian biểu. Eg: The plan **arrives** at 18:00 tomorrow.

1.2 Công thức

A.Động từ To Be
- Thể khẳng định
 - I+am
 - He+is
 - She+is
 - It+is
 - We+are
 - You+are
 - They+are
 
 Eg: I am a student 

It's very hot today

They are my aunts and uncles

- Thể phủ định: trong thể phủ định ta chỉ cần thêm **NOT** vào phía sau động từ ** TO BE** được chia theo chủ ngữ.
- I + am not == I'm not 
- You + are not == You're not == You aren't
- He + is not == He's not == he isn't
- She + is not == She's not == She isn't
- It + is not == It's not == It isn't
- We + are not == We're not == We aren't
- You + are not == You're not == You aren't 
- They + are not == They're not == They aren't
Eg:

I **am not** a teacher

He **is not** tall

We **are not** friends

Sự khác nhau của rút gọn 1 và  rút gọn 2 là: Dạng rút gọn 1 nhấn mạnh tính chất **KHÔNG PHẢI** (**NOT**), còn rút gọn 2 chỉ đơn thuần là ohur định

- Thể nghi vấn: Đối với câu hỏi Wh-question, đầu tiên ta đặt từ hỏi (what, which, who, when, whose where, why, how) và đảo **TO BE** đẵ được chia ra trước chủ từ. Đối với câu hỏi Yes/No, ta đảo **TO BE** đã được chia động từ ra trước chủ từ.
- Từ để hỏi + am + I
- Từ để hỏi + are + you
- Từ để hỏi + is + he
- Từ để hỏi + is + she
- Từ để hỏi + is + it
- Từ để hỏi + are + we
- Từ để hỏi + are + you
- Từ để hỏi + are + they

Eg:

Where **are** you from 

Where **is** the book

Why **isn't** he here (Câu nghi vấn dạng phủ định)

**Are** you home now

**Is** the movie

**Isn't** she a dancer

B. Động Từ Thường

- Khẳng định: Đối với các ngôi "I, you, we, they" chúng ta sẽ dùng động từ nguyên mẫu đi kèm với thể khẳng định. Nhưng lưu ý đối với các ngôi "he, she, it", động từ sẽ bị biết đổi một chút xíu ở thể khẳng định.

Chủ ngữ + chia động từ
I + work
You + work
He + works
She + works
It + works
We + works
You + work
They + work

Eg:
The students take the bus to school every morning
I have two dogs at home
Jordan plays basketball very well

- Phủ định : trong thể phủ định với động từ thường ta sẽ mượn **trợ động từ** "do" cho chủ từ "i, you,we, they"hoặc "does" cho chủ từ "he, she, it" trước nó động từ nguyên mẫu như sau:

Chủ từ + trợ động từ "do" + "not" + đọng từ nguyên mẫu

I + do not + work 

You + do not + work

he + does not + work

she + does not + work

it + does not + work

We + do not + work

you + do not + work

they + do not + wok

Eg: 
Harry doesn't take a nap every day (nap=giấc trưa)
We don't like playing chess (chess = cờ vua)
My father doesn't work at weekends

- Câu nghi vấn: Đối với câu hỏi Wh-question, đầu tiên ta đặt câu hỏi (what, which, who, where, when, Whose, Why, how), đặt trợ đọng từ "do" đã được chia động từ ra trước chủ từ và đọng từ nguyên mẫu sau chủ từ.
- Đối với câu hỏi Yes/No , ta đặt trợ động từ "do" đăc được chia ra trước chủ từ, và động từ nguyên mẫu ra sau chủ từ.

(Từ để hỏi) + Trợ động từ "do" + chủ từ + động từ nguyên mẫu

(Từ để hỏi) + do + I + work

(Từ để hỏi) + do + you +work

(Từ để hỏi ) + does + he + work 

( Từ để hỏi)+ does + she + work 

( Từ để hỏi) + does + it + work

(Từ để hỏi) + do + we + work

(Từ để hỏi) + do +you + work

(Từ để hỏi) + do + they + work

Eg:

What time **does** Bob **have** dinner

How **does** she go to school

When **do** they **go** to the cinema

C. Tổng kết

<img src="https://tienganhmoingay.com/media/images/uploads/2018/09/15/work_4.png">

D. Quy tắc biến đổi động từ của thi hiện tại đơn ở ngôi thứ 3 số ít

- Thêm -es cho các động từ có tận cùng là -o, -s, -ch, -sh, -x, -z (Tạm đọc : Ốc Sên Chạy Xe SH Zỏm)

- Đố với động từ có tận cùng bằng -y
 
 - Trường hợp một: chúng ta sẽ chuyển -y thành -ies nếu trước -y là một phụ âm
 - Trường hợp hai: Chúng ta sẽ thêm -s vào sau động từ nếu  nếu trước -y là một nguyên âm 

- Đố với động từ **have** chúng ta chia thành **has* cho chủ ngữ ngôi thứ 3 số ít

- Với tất cả các động từ còn lại chúng ta chỉ cần thêm **s** vào sau động từ cần chia.

E. Dấu hiệu

Thì hiện tại đơn thường đi với : every day , sometimes, always, often, usually, seldom, never, first ... then



